/*
 * Getting_Started_with_AVR_Timer_Overflow_IRQs.c
 *
 * Created: 3/27/2015 0:44:06
 *  Author: Brandy
 */ 

#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

ISR(TIMER1_OVF_vect)
{
	PORTB |= (1<<PINB0);
}
int main(void)
{
	DDRB |= (1 << DDB0);
	DDRB &= ~(1 << DDB1);
	sei();
	TCCR1 |= (0 << CS13)|(0 << CS12)|(1 << CS11)|(1 << CS10);
	/*
		TCCR1 � Timer/Counter1 Control Register
		CTC1 PWM1A COM1A1 COM1A0 CS13 CS12 CS11 CS10
		Clear Timer/Counter on Compare Match
		Pulse Width Modulator A Enable
		Comparator A Output Mode, Bits 1 and 0
		Clock Select Bits 3, 2, 1, and 0
			The Clock Select bits 3, 2, 1, and 0 define the prescaling source of Timer/Counter1.
			CS11 and CS10 as 1 is the prescaler 102 so the Timer Will Overflow Every 1.907seconds
		pg89
	*/
	TIMSK |=(1 <<TOIE1);
	/*
		Timer/Counter Interrupt Mask Register
		� OCIE1A OCIE1B OCIE0A OCIE0B TOIE1 TOIE0 � 
		Reserved Bit
		Timer/Counter1 Output Compare Interrupt Enable
		Timer/Counter1 Output Compare Interrupt Enable
		Timer/Counter1 Overflow Interrupt Enable
			When the TOIE1 bit is set (one) and the I-bit in the Status Register is set (one), the Timer/Counter1 Overflow interrupt is enabled. 
			The corresponding interrupt (at vector $004) is executed if an overflow in Timer/Counter1 occurs.
			The Overflow Flag (Timer1) is set (one) in the Timer/Counter Interrupt Flag Register - TIFR.
		Reserved Bit
		pg92
	*/
    while(1)
    {
        //TODO:: Please write your application code 
    }
}